﻿using System.Xml.Schema;

namespace WebApplication1.EntityModel
{
    public class OrderEntity
    {
        public int ID { get; set; }
        public string ProductName { get; set; }
        public int ProductId { get; set; }
        public int Price { get; set; }
        public int Quantity { get; set; }
        public int Total { get; set; }
       
    }
}
