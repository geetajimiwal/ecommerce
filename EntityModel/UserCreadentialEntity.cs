﻿namespace WebApplication1.EntityModel
{
    public class UserCreadentialEntity
    {
        public int ID { get; set; }
        public int UserId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
