2023-03-07 14:51:38.600 +05:30 [INF] Request starting HTTP/2 GET https://localhost:7097/Api/Product - -
2023-03-07 14:51:38.621 +05:30 [INF] CORS policy execution successful.
2023-03-07 14:51:38.622 +05:30 [INF] Executing endpoint 'WebApplication1.Controllers.ProductController.GetAll (WebApplication1)'
2023-03-07 14:51:38.622 +05:30 [INF] Route matched with {action = "GetAll", controller = "Product"}. Executing controller action with signature System.Collections.Generic.IEnumerable`1[WebApplication1.EntityModel.ProductEntity] GetAll() on controller WebApplication1.Controllers.ProductController (WebApplication1).
2023-03-07 14:51:38.644 +05:30 [INF] Executed DbCommand (13ms) [Parameters=[], CommandType='"Text"', CommandTimeout='30']
SELECT [p].[Id], [p].[Category], [p].[Image], [p].[Name], [p].[Price], [p].[Quantity]
FROM [Products] AS [p]
2023-03-07 14:51:38.645 +05:30 [INF] Executing ObjectResult, writing value of type 'System.Collections.Generic.List`1[[WebApplication1.EntityModel.ProductEntity, WebApplication1, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null]]'.
2023-03-07 14:51:38.646 +05:30 [INF] Executed action WebApplication1.Controllers.ProductController.GetAll (WebApplication1) in 23.846ms
2023-03-07 14:51:38.646 +05:30 [INF] Executed endpoint 'WebApplication1.Controllers.ProductController.GetAll (WebApplication1)'
2023-03-07 14:51:38.646 +05:30 [INF] Request finished HTTP/2 GET https://localhost:7097/Api/Product - - - 200 - application/json;+charset=utf-8 45.4860ms
2023-03-07 14:51:40.211 +05:30 [INF] Request starting HTTP/2 GET https://localhost:7097/api/CartItem - -
2023-03-07 14:51:40.211 +05:30 [INF] Request starting HTTP/2 GET https://localhost:7097/Api/Product - -
2023-03-07 14:51:40.211 +05:30 [INF] CORS policy execution successful.
2023-03-07 14:51:40.211 +05:30 [INF] CORS policy execution successful.
2023-03-07 14:51:40.212 +05:30 [INF] Executing endpoint 'WebApplication1.Controllers.ProductController.GetAll (WebApplication1)'
2023-03-07 14:51:40.212 +05:30 [INF] Executing endpoint 'WebApplication1.Controllers.CartItemController.cart (WebApplication1)'
2023-03-07 14:51:40.212 +05:30 [INF] Route matched with {action = "GetAll", controller = "Product"}. Executing controller action with signature System.Collections.Generic.IEnumerable`1[WebApplication1.EntityModel.ProductEntity] GetAll() on controller WebApplication1.Controllers.ProductController (WebApplication1).
2023-03-07 14:51:40.212 +05:30 [INF] Route matched with {action = "cart", controller = "CartItem"}. Executing controller action with signature Microsoft.AspNetCore.Http.IResult cart(WebApplication1.EntityModel.CartItemEntity) on controller WebApplication1.Controllers.CartItemController (WebApplication1).
2023-03-07 14:51:40.213 +05:30 [INF] Executing ObjectResult, writing value of type 'Microsoft.AspNetCore.Mvc.ProblemDetails'.
2023-03-07 14:51:40.213 +05:30 [INF] Executed action WebApplication1.Controllers.CartItemController.cart (WebApplication1) in 1.1481ms
2023-03-07 14:51:40.213 +05:30 [INF] Executed endpoint 'WebApplication1.Controllers.CartItemController.cart (WebApplication1)'
2023-03-07 14:51:40.213 +05:30 [INF] Executed DbCommand (1ms) [Parameters=[], CommandType='"Text"', CommandTimeout='30']
SELECT [p].[Id], [p].[Category], [p].[Image], [p].[Name], [p].[Price], [p].[Quantity]
FROM [Products] AS [p]
2023-03-07 14:51:40.213 +05:30 [INF] Request finished HTTP/2 GET https://localhost:7097/api/CartItem - - - 415 - application/json;+charset=utf-8 2.3449ms
2023-03-07 14:51:40.214 +05:30 [INF] Executing ObjectResult, writing value of type 'System.Collections.Generic.List`1[[WebApplication1.EntityModel.ProductEntity, WebApplication1, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null]]'.
2023-03-07 14:51:40.214 +05:30 [INF] Executed action WebApplication1.Controllers.ProductController.GetAll (WebApplication1) in 2.2574ms
2023-03-07 14:51:40.214 +05:30 [INF] Executed endpoint 'WebApplication1.Controllers.ProductController.GetAll (WebApplication1)'
2023-03-07 14:51:40.214 +05:30 [INF] Request finished HTTP/2 GET https://localhost:7097/Api/Product - - - 200 - application/json;+charset=utf-8 3.1329ms
2023-03-07 14:51:48.268 +05:30 [INF] Request starting HTTP/2 GET https://localhost:7097/Api/Product/Men Fashion?product=Men%20Fashion - -
2023-03-07 14:51:48.268 +05:30 [INF] CORS policy execution successful.
2023-03-07 14:51:48.268 +05:30 [INF] Executing endpoint 'WebApplication1.Controllers.ProductController.GetByCategory (WebApplication1)'
2023-03-07 14:51:48.270 +05:30 [INF] Route matched with {action = "GetByCategory", controller = "Product"}. Executing controller action with signature System.Collections.Generic.List`1[WebApplication1.EntityModel.ProductEntity] GetByCategory(System.String) on controller WebApplication1.Controllers.ProductController (WebApplication1).
2023-03-07 14:51:48.293 +05:30 [INF] Executed DbCommand (2ms) [Parameters=[@__product_0='?' (Size = 4000)], CommandType='"Text"', CommandTimeout='30']
SELECT [p].[Id], [p].[Category], [p].[Image], [p].[Name], [p].[Price], [p].[Quantity]
FROM [Products] AS [p]
WHERE [p].[Category] = @__product_0
2023-03-07 14:51:48.293 +05:30 [INF] Executing ObjectResult, writing value of type 'System.Collections.Generic.List`1[[WebApplication1.EntityModel.ProductEntity, WebApplication1, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null]]'.
2023-03-07 14:51:48.294 +05:30 [INF] Executed action WebApplication1.Controllers.ProductController.GetByCategory (WebApplication1) in 23.3383ms
2023-03-07 14:51:48.294 +05:30 [INF] Executed endpoint 'WebApplication1.Controllers.ProductController.GetByCategory (WebApplication1)'
2023-03-07 14:51:48.294 +05:30 [INF] Request finished HTTP/2 GET https://localhost:7097/Api/Product/Men Fashion?product=Men%20Fashion - - - 200 - application/json;+charset=utf-8 26.6503ms
2023-03-07 14:51:59.462 +05:30 [INF] Request starting HTTP/2 OPTIONS https://localhost:7097/api/CartItem - -
2023-03-07 14:51:59.462 +05:30 [INF] CORS policy execution successful.
2023-03-07 14:51:59.463 +05:30 [INF] Request finished HTTP/2 OPTIONS https://localhost:7097/api/CartItem - - - 204 - - 0.4440ms
2023-03-07 14:51:59.466 +05:30 [INF] Request starting HTTP/2 POST https://localhost:7097/api/CartItem application/json 89
2023-03-07 14:51:59.467 +05:30 [INF] CORS policy execution successful.
2023-03-07 14:51:59.467 +05:30 [INF] Executing endpoint 'WebApplication1.Controllers.CartItemController.cart (WebApplication1)'
2023-03-07 14:51:59.467 +05:30 [INF] Route matched with {action = "cart", controller = "CartItem"}. Executing controller action with signature Microsoft.AspNetCore.Http.IResult cart(WebApplication1.EntityModel.CartItemEntity) on controller WebApplication1.Controllers.CartItemController (WebApplication1).
2023-03-07 14:51:59.471 +05:30 [INF] Executed DbCommand (3ms) [Parameters=[@__id_0='?' (DbType = Int32)], CommandType='"Text"', CommandTimeout='30']
SELECT TOP(1) [c].[Id], [c].[Image], [c].[Price], [c].[ProductId], [c].[ProductName], [c].[Quantity], [c].[Total]
FROM [CartItem] AS [c]
WHERE [c].[ProductId] = @__id_0
2023-03-07 14:51:59.472 +05:30 [INF] Executed DbCommand (0ms) [Parameters=[@__id_0='?' (DbType = Int32)], CommandType='"Text"', CommandTimeout='30']
SELECT TOP(1) [c].[Id], [c].[Image], [c].[Price], [c].[ProductId], [c].[ProductName], [c].[Quantity], [c].[Total]
FROM [CartItem] AS [c]
WHERE [c].[ProductId] = @__id_0
2023-03-07 14:51:59.473 +05:30 [INF] Executed DbCommand (1ms) [Parameters=[@__cartItem_ProductId_0='?' (DbType = Int32)], CommandType='"Text"', CommandTimeout='30']
SELECT TOP(1) [p].[Id], [p].[Category], [p].[Image], [p].[Name], [p].[Price], [p].[Quantity]
FROM [Products] AS [p]
WHERE [p].[Id] = @__cartItem_ProductId_0
2023-03-07 14:51:59.478 +05:30 [INF] Executed DbCommand (4ms) [Parameters=[@p6='?' (DbType = Int32), @p0='?' (Size = 4000), @p1='?' (DbType = Int32), @p2='?' (DbType = Int32), @p3='?' (Size = 4000), @p4='?' (DbType = Int32), @p5='?' (DbType = Int32)], CommandType='"Text"', CommandTimeout='30']
SET IMPLICIT_TRANSACTIONS OFF;
SET NOCOUNT ON;
UPDATE [CartItem] SET [Image] = @p0, [Price] = @p1, [ProductId] = @p2, [ProductName] = @p3, [Quantity] = @p4, [Total] = @p5
OUTPUT 1
WHERE [Id] = @p6;
2023-03-07 14:51:59.478 +05:30 [INF] Executing ObjectResult, writing value of type 'Microsoft.AspNetCore.Http.Result.OkObjectResult'.
2023-03-07 14:51:59.478 +05:30 [INF] Executed action WebApplication1.Controllers.CartItemController.cart (WebApplication1) in 11.1394ms
2023-03-07 14:51:59.478 +05:30 [INF] Executed endpoint 'WebApplication1.Controllers.CartItemController.cart (WebApplication1)'
2023-03-07 14:51:59.478 +05:30 [INF] Request finished HTTP/2 POST https://localhost:7097/api/CartItem application/json 89 - 200 - application/json;+charset=utf-8 12.3726ms
2023-03-07 14:51:59.631 +05:30 [INF] Request starting HTTP/2 GET https://localhost:7097/api/CartItem - -
2023-03-07 14:51:59.631 +05:30 [INF] CORS policy execution successful.
2023-03-07 14:51:59.631 +05:30 [INF] Executing endpoint 'WebApplication1.Controllers.CartItemController.cart (WebApplication1)'
2023-03-07 14:51:59.631 +05:30 [INF] Route matched with {action = "cart", controller = "CartItem"}. Executing controller action with signature Microsoft.AspNetCore.Http.IResult cart(WebApplication1.EntityModel.CartItemEntity) on controller WebApplication1.Controllers.CartItemController (WebApplication1).
2023-03-07 14:51:59.631 +05:30 [INF] Executing ObjectResult, writing value of type 'Microsoft.AspNetCore.Mvc.ProblemDetails'.
2023-03-07 14:51:59.632 +05:30 [INF] Executed action WebApplication1.Controllers.CartItemController.cart (WebApplication1) in 0.6219ms
2023-03-07 14:51:59.632 +05:30 [INF] Executed endpoint 'WebApplication1.Controllers.CartItemController.cart (WebApplication1)'
2023-03-07 14:51:59.632 +05:30 [INF] Request finished HTTP/2 GET https://localhost:7097/api/CartItem - - - 415 - application/json;+charset=utf-8 1.1765ms
