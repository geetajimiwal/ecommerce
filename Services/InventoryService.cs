﻿using WebApplication1.Model;
using WebApplication1.Repository;

namespace WebApplication1.Services
{
    public class InventoryService : INventoryInterface
    {
        private readonly INventoryRepoInterface inventoryInterface;
        public InventoryService(INventoryRepoInterface inventoryInterface)
        {
            this.inventoryInterface = inventoryInterface;
        }

        public Inventory Create(Inventory inventory)
        {
            inventory.IsActive = true;
            inventory.CreatedAt = DateTime.Now;
            inventory.UpdatedAt = DateTime.Now;
            inventory.UpdatedBy = 1;
            inventory.CreatedBy = 1;

            inventory = inventoryInterface.Create(inventory);
            return inventory;
        }

        public Inventory Delete(Inventory inventory)
        {
            throw new NotImplementedException();
        }

        public List<Inventory> GetAll()
        {
             var GetAllInventory = inventoryInterface.GetAll();
            return GetAllInventory;
        }

        public Inventory GetById(int id)
        {
             var Inventory = inventoryInterface.GetById(id);
            return Inventory;
        }

        public Inventory Update(Inventory inventory)
        {
            throw new NotImplementedException();
        }
    }
}
