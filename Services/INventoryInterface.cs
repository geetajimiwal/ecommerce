﻿using WebApplication1.Model;

namespace WebApplication1.Services
{
    public interface INventoryInterface
    {
        Inventory GetById(int id);
        List<Inventory> GetAll();
        Inventory Create(Inventory inventory);
        Inventory Update(Inventory inventory);
        Inventory Delete(Inventory inventory);
    }
}
