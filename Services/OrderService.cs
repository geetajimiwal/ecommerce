﻿using WebApplication1.Model;
using WebApplication1.Repository;

namespace WebApplication1.Services
{
    public class OrderService : IOrderService
    {

        private readonly IOrderRepository orderRepository;
        public OrderService(IOrderRepository orderRepository)
        {
            this.orderRepository = orderRepository;
        }

        public OrderProduct Create(OrderProduct orderProduct)
        {
             orderProduct.ProductId = 1;
            orderProduct.ProductName = orderProduct.ProductName;
            orderProduct.Price = orderProduct.Price;
            orderProduct.Total = orderProduct.Total;
            var order = orderRepository.Create(orderProduct);
            return order;
        }



        /* public OrderProduct Delete(int Id)
         {
             throw new NotImplementedException();
         }
         public OrderProduct Create(OrderProduct product)
         {
             var order = orderRepository.Create(product);
             return order;
         }
         public List<OrderProduct> GetAll()
         {

              var order = orderRepository.GetAll();
             return order;
         }

         public OrderProduct GetById(int Id)
         {

             var order = orderRepository.GetById(Id);
             return order;
         }

         public OrderProduct Update(int Id)
         {
             throw new NotImplementedException();
         }
 */

    }
}
