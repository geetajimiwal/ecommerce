﻿using WebApplication1.Model;
namespace WebApplication1.Repository
{
    public interface INventoryRepoInterface
    {
        Inventory GetById(int id);
        List<Inventory> GetAll();
        Inventory Create(Inventory inventory);
        Inventory Update(Inventory inventory);
        Inventory Delete(Inventory inventory);
    }
}
