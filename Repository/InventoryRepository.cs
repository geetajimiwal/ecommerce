﻿using Microsoft.EntityFrameworkCore;
using WebApplication1.Db;
using WebApplication1.Model;

namespace WebApplication1.Repository
{
    public class InventoryRepository :INventoryRepoInterface
    {
        private readonly ProductContext productContext;
        private readonly DbSet<Inventory> inventorySet;

        public InventoryRepository(ProductContext productContext)
        {
            this.productContext = productContext;
            inventorySet =  productContext.Set<Inventory>();
        }

        public Inventory Create(Inventory inventory)
        {
            productContext.Add(inventory);
            productContext.SaveChanges();
            return inventory;
        }

        public Inventory Delete(Inventory inventory)
        {
            throw new NotImplementedException();
        }

        public List<Inventory> GetAll()
        {
            return this.inventorySet.AsQueryable().ToList();
        }

        public Inventory GetById(int id)
        {
            return inventorySet.Find(id);
        }

        public Inventory Update(Inventory inventory)
        {
            throw new NotImplementedException();
        }
    }
}
