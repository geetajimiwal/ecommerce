﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualBasic;
using Org.BouncyCastle.Utilities;
using System.Linq;
using System.Net;
using WebApplication1.Db;
using WebApplication1.EntityModel;
using WebApplication1.Model;
using WebApplication1.Services;

namespace WebApplication1.Repository
{
    public class CartItemRepository : ICartItemInterface
    {
        private readonly ProductContext dbContext;
        private readonly DbSet<CartItem> cartItems;
        public CartItemRepository(ProductContext dbContext)
        {
            this.dbContext = dbContext;
            this.cartItems = dbContext.Set<CartItem>();
        }

        public CartItem Create(CartItem cartItem)
        {
            dbContext.Add(cartItem);
            dbContext.SaveChanges();
            return cartItem;
        }
        
        public CartItem Update(CartItem cartItem)
        {
            var cartDataById = GetById(cartItem.ProductId);
           var productquentity = dbContext.Products.FirstOrDefault(p => p.Id == cartItem.ProductId);
            if (cartDataById.Quantity <= productquentity.Quantity)
            {
               /* cartDataById.Quantity = cartDataById.Quantity + 1;*/
                cartDataById.Price = cartDataById.Quantity *Convert.ToInt32(productquentity.Price);
              /*  cartDataById.Total = cartDataById.Price + Convert.ToInt32(productquentity);*/

                /* cartDataById.Price = dbContext.CartItem.Sum(p => p.Price *p.Quantity);*/



                /*cartDataById.Total = cartItem.Price + cartItem.Price;
                cartDataById.Sum(t => t.cartItem.Price);*/
                dbContext.Update(cartDataById);
                dbContext.SaveChanges();
                return cartDataById;
            }
            return null;
        }
        public CartItem Total(CartItem total)
        {

            var totalPrice = 0;
            foreach (var product in cartItems)
            {
                totalPrice += product.Price;
            }
            total.Total = totalPrice;
            return total;
        }
            
       


        public CartItem Delete(int id)
        {
            throw new NotImplementedException();
        }

        public List<CartItem> GetAll()
        { 
            return this.cartItems.AsQueryable().ToList();
        }

        public CartItem GetById(int id)
        {
            /*   return cartItems.Find(id);*/
            var cardDatabyID = dbContext.CartItem.Where(x => x.ProductId == id).FirstOrDefault();
            return cardDatabyID;
        }

        public CartItem ProductName(CartItem productName)
        {
            throw new NotImplementedException();
        }

      

        public CartItem DeleteProduct(int id)
        {
            var deleteProduct = this.cartItems.Find(id);
            if (deleteProduct != null)
            {
                dbContext.Remove(deleteProduct);
                dbContext.SaveChanges();
            }
            return deleteProduct;
        }

        
    }
}
