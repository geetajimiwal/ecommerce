﻿using System.ComponentModel.DataAnnotations.Schema;

namespace WebApplication1.Model
{
    public class OrderProduct
    {
       public int ID { get; set; }
        public string ProductName { get; set; }
        public int ProductId { get; set; }
        public int Price { get; set; }
        public int Quantity { get; set; }
        public int Total { get; set; }
       

    }
}
