﻿namespace WebApplication1.Model
{
    public class CartItem
    {
        public int Id { get; set; }
        public string? ProductName { get; set; }
        public int Quantity { get; set; }
        public int Price { get; set; } 
        public int ProductId { get; set; }
        public string ?Image { get; set; }
        public int Total { get; set; }

        internal int reduce(Func<object, object, object> value, int v)
        {
            throw new NotImplementedException();
        }
    }
}
