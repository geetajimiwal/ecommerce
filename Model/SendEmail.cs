﻿namespace WebApplication1.Model
{
    public class SendEmail
    {
        public string ApiKey { get; set; }

        public string SenderEmail { get; set; }

        public string SenderName { get; set; }
    }
}
