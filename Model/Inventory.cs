﻿namespace WebApplication1.Model
{
    public class Inventory:BaseClass
    {
      
        public string Product { get; set; }
        public int Quantity { get; set; }
        public int Price { get; set; }
        
    }
}
