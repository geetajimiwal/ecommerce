﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using WebApplication1.EntityModel;
using WebApplication1.Model;
using WebApplication1.Services;
namespace WebApplication1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : Controller
    {

        private readonly ICartItemService cartItemService;
        private readonly IMapper mapper;
        private readonly IOrderService orderService;

        public OrderController(ICartItemService cartItemService, IMapper mapper, IOrderService orderService)
        {
            this.cartItemService = cartItemService;
            this.mapper = mapper;
            this .orderService = orderService;
        }
        
        [HttpPost]
        [AllowAnonymous]
        [Produces("application/json")]
        public async Task<IResult> Post(OrderEntity orderEntity)
        {
            try
            {
                
                
                var orderMap = mapper.Map<OrderProduct>(orderEntity);
                var orderSummry = orderService.Create(orderMap);
              /*  cartItemService.Create(orderSummry.Price, orderSummry.ProductName);*/
                if (orderSummry != null)
                {
                    return Results.Ok(orderEntity.Price);
                }
                return Results.BadRequest();
            }
            catch (Exception e)
            {
                throw;
            }

        }

    }
}
