﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using WebApplication1.EntityModel;
using WebApplication1.Model;
using WebApplication1.Services;

namespace WebApplication1.Controllers
{
    [Route("Api/[controller]")]
    [ApiController]
    public class InventoryController : Controller
    {
        private readonly INventoryInterface inventoryInterface;
        public InventoryController (INventoryInterface inventoryInterface)
        {
            this.inventoryInterface = inventoryInterface;
        }
        [HttpGet]
        public IEnumerable<Inventory> GetAll()
        {
            var products = inventoryInterface.GetAll();
            return products;
        }
        [HttpGet("{id}")]
        public List<Inventory> Get(int id)
        {
            var productById = new List<Inventory>();
            var products = inventoryInterface.GetAll();
            
            foreach (var product in products)
            {
                if (product.Id == id)
                {
                    productById.Add(product);
                }
            }
            return productById;
        }
       

        [HttpPost]
        public async void Create(Inventory inventory)
        {
           inventoryInterface.Create(inventory);
            
           
        }
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {

        }
        [HttpDelete("{id}")]
        public void Delete(int id)
        {

        }

    }
}
