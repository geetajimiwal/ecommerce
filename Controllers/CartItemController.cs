﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using WebApplication1.Db;
using WebApplication1.EntityModel;
using WebApplication1.Model;
using WebApplication1.Services;

namespace WebApplication1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CartItemController : Controller
    {
        private readonly ICartItemService cartItemService;
        private readonly IMapper mapper;
        private readonly IProductService productService;

        public CartItemController(ICartItemService cartItemService, IMapper mapper, IProductService productService)
        {
            this.cartItemService = cartItemService;
            this.mapper = mapper;
            this.productService = productService;
        }
        /*[HttpGet]
        public IEnumerable<CartItemEntity> GetAll()
        {
            var carts = cartItemService.GetAll();
            var cartEntity = mapper.Map<List<CartItemEntity>>(carts);
            return cartEntity;
        }*/
        [HttpGet("{id}")]
        public List<CartItemEntity> Get(int id)
        {
            var cartById = new List<CartItemEntity>();
            var carts = cartItemService.GetAll();
            var cartEntity = mapper.Map<List<CartItemEntity>>(carts);
            foreach (var cart in cartEntity)
            {
                if (cart.Id == id)
                {
                    cartById.Add(cart);
                }
            }
            return cartById;
        }
        [HttpGet]
/*        public IResult GetTotal(CartItem total)
        {
            var carts = cartItemService.Total(total);
            return carts;
        }*/
        [HttpPost]
        [AllowAnonymous]
        [Produces("application/json")]
        public IResult cart(CartItemEntity cartItemEntity)
        {
            
                var data = cartItemService.GetById(cartItemEntity.ProductId);
                var catitemsummry = mapper.Map<CartItem>(cartItemEntity);
                if (data != null)
                {
                    var cartitemview = cartItemService.Update(catitemsummry);
                    return Results.Ok(cartitemview);
                }
                else if(data== null)
                {
                    var cartitemview = cartItemService.Create(catitemsummry);
                    return Results .Ok(cartitemview);
                }


                else
                {
                    return Results.BadRequest();
                }



        }  
            

        [HttpPut]
        public  IResult UpdateProduct(CartItemEntity cartItemEntity)
        {

            var data = cartItemService.GetById(cartItemEntity.ProductId);
            data.Quantity = cartItemEntity.Quantity;
            data.Price = cartItemEntity.Price;
            var cartsdelete = cartItemService.Update(data);
            if (cartsdelete != null)
            {
                Results.Ok(cartsdelete.Quantity);
            }
            else
            {
                return Results.BadRequest();
            }
            return Results.Ok();
         

        }

        [HttpDelete("{id}")]
        public CartItem delete(int id)
        {
             
            var cartsdelete = cartItemService.DeleteProduct(id);
            return cartsdelete;

        }

        /*public async Task<CartItemEntity> DeleteProduct(int id)
        {
            var cartById = new List<CartItemEntity>();
            var cartsdelete = cartItemService.DeleteProduct(id);
            var cartEntityDelete = mapper.Map<List<CartItem>>(cartsdelete);
            return cartEntityDelete;
        }*/
    }
}
 
